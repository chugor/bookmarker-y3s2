import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import firebase from "firebase";

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const firebaseAuth = firebase.auth();
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  // Nav Guards, prevent unauth.ed access to specific page
  Router.beforeEach((to, from, next) => {
    // check for requiresAuth guard (see if the path have requiresAuth: true)
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // check if NOT logged in
      if (!firebaseAuth.currentUser) {
        // go to login page (index /)
        next({
          path: "/",
          query: {
            redirect: to.fullPath // dont know do what (-.-") . . . . . .
          }
        });
      } else {
        // proceed the route
        next();
      }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
      // check if ALREADY logged in
      if (firebaseAuth.currentUser) {
        // go to login page (index /)
        next({
          path: "/book", // in youtube tut is "/", but it will maximum call stack error, so i changed it to go to /book
          query: {
            redirect: to.fullPath // dont know do what (-.-") . . . . . .
          }
        });
      } else {
        // proceed the route
        next();
      }
    } else {
      // default case
      next();
    }
  });

  return Router
}
