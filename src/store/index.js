import Vue from 'vue'
import Vuex from 'vuex'
import firebase from "firebase"
import db from "boot/firebaseInit";
import { store } from 'quasar/wrappers';

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const firebaseAuth = firebase.auth();
  const firebaseDB = firebase.database();
  const compareObj = key => {
    return (objA, objB) => objA[key] < objB[key] ? -1 : objA[key] === objB[key] ? 0 : 1
  }
  const isEmptyArray = a => a.length === 0;
  const isIn = (t, a) => a.indexOf(t) !== -1;
  const Store = new Vuex.Store({
    modules: {
      // example
    },

    state: {
      foo: "hello",
      userInfo: {
        id: "",
        name: "",
        email: ""
      },
      bookmarks: [],
      queryOption: {
        name: "",
        isShowFavouriteOnly: false,
        isSortDateAsc: false,
        sortName: "close",
        targetTags: []
      }
    },
    getters: {
      userInfo(state) {
        return state.userInfo
      },
      userDoc(state) {
        return db.collection("bookmarkerDB").doc(state.userInfo.id)
      },
      bookmarks(state) {
        // default case: sort by date (newest to oldest)
        let bookmarksBuff = state.bookmarks;

        bookmarksBuff = bookmarksBuff.sort((objA, objB) => {
          const front = state.queryOption.isSortDateAsc ? -1 : 1;
          const back = state.queryOption.isSortDateAsc ? 1 : -1;

          if (state.queryOption.sortName !== "close") {
            if (state.queryOption.sortName === "asc") {
              if (objA.name < objB.name) return -1;
              if (objA.name > objB.name) return 1;
            } else {
              if (objA.name < objB.name) return 1;
              if (objA.name > objB.name) return -1;
            }
          }
          if (objA.createdTime.year < objB.createdTime.year) return front;
          if (objA.createdTime.year > objB.createdTime.year) return back;
          if (objA.createdTime.month < objB.createdTime.month) return front;
          if (objA.createdTime.month > objB.createdTime.month) return back;
          if (objA.createdTime.date < objB.createdTime.date) return front;
          if (objA.createdTime.date > objB.createdTime.date) return back;
          if (objA.createdTime.hours < objB.createdTime.hours) return front;
          if (objA.createdTime.hours > objB.createdTime.hours) return back;
          if (objA.createdTime.minutes < objB.createdTime.minutes) return front;
          if (objA.createdTime.minutes > objB.createdTime.minutes) return back;
          if (objA.createdTime.seconds < objB.createdTime.seconds) return front;
          if (objA.createdTime.seconds > objB.createdTime.seconds) return back;
          //TODO: add sort name here?
          return 0;
        })

        // filter isShowFavouriteOnly
        if (state.queryOption.isShowFavouriteOnly) bookmarksBuff = bookmarksBuff.filter(bookmark => bookmark.isFavourite);

        // filter containing name
        // check empty name
        if (state.queryOption.name !== "" && !((state.queryOption.name.split(" ").length - 1) === state.queryOption.name.length)) {
          let targetKeywords = state.queryOption.name
            .split(" ")
            .filter(keyword => !!keyword)
            .map(keyword => keyword.replace(/[.,\/#!$%\^&\*;:{}@=\-_`~()'"]/g, ""));
          
          bookmarksBuff = bookmarksBuff.filter(bookmark => {
            let bookmarkKeywords = bookmark.name
              .split(" ")
              .filter(keyword => !!keyword)
              .map(keyword => keyword.replace(/[.,\/#!$%\^&\*;:{}@=\-_`~()'"]/g, ""));
            
            // check if have contain any keyword
            for (let i = 0; i < targetKeywords.length; i++) {
              const targetKeywordRegExp = new RegExp(targetKeywords[i], "g");

              for (let j = 0; j < bookmarkKeywords.length; j++) {
                if (bookmarkKeywords[j].match(targetKeywordRegExp)) return true;
              }
            }
            return false;
          });
        }

        // filter contain any target tag
        if (!isEmptyArray(state.queryOption.targetTags)) {
          state.queryOption.targetTags
            .map(tag => bookmarksBuff = bookmarksBuff
              .filter(bookmark => isIn(tag, bookmark.tags))
            );
        }

        console.log("state.queryOption", state.queryOption);
        console.log(bookmarksBuff);
        return bookmarksBuff;
      }
    },
    mutations: {
      setUserInfo(state, newUserInfo) {
        console.log("setUserInfo");
        state.userInfo = newUserInfo;
      },
      clearUserInfo(state) {
        console.log("clearUserInfo");
        state.userInfo = {
          id: "",
          name: "",
          email: ""
        };
      },
      setIsSortDateAsc(state, boolVal) {
        state.queryOption.isSortDateAsc = boolVal;
      },
      setIsShowFavouriteOnly(state, boolVal) {
        state.queryOption.isShowFavouriteOnly = boolVal;
      },
      setQueryOptionName(state, name) {
        state.queryOption.name = name;
      },
      setQueryOption(state, newQueryOption) {
        state.queryOption.name = newQueryOption.targetName;
        state.queryOption.sortName = newQueryOption.sortName;
        state.queryOption.targetTags = newQueryOption.targetTagsStr.split(",")
          .map(tag => tag.split(" "))
          .map(tagArray => tagArray.filter(tag => tag !== ""))
          .map(tagArray => {
            let tag = tagArray[0];

            if (tagArray.length > 1) {
              for (let i = 1; i < tagArray.length; i++) {
                tag += " " + tagArray[i];
              }
            }
            return tag;
          })
          .filter(tag => !!tag)
          .filter(tag => !tag.match(/\d/g))
          .filter(tag => !tag.match(/[.,\/#!$%\^&\*;:{}@=\-_`~()'"]/g));
      },
      initQueryOption(state) {
        state.queryOption = {
          name: "",
          isShowFavouriteOnly: false,
          isSortDateAsc: false,
          sortName: "close",
          targetTags: []
        };
      }
    },
    actions: {
      registerUser(state, registData) {
        console.log("registerUser");
        console.log("registData", registData);
        firebaseAuth.createUserWithEmailAndPassword(registData.email, registData.password)
          .then(res => {
            const userID = firebaseAuth.currentUser.uid;

            console.log("res", res);
            firebaseDB.ref("users/" + userID).set({
              name: registData.username,
              email: registData.email
            });
          })
          .catch(err => {
            console.error(err);
          });
      },
      loginUser(state, loginData) {
        console.log("loginData");
        firebaseAuth.signInWithEmailAndPassword(loginData.email, loginData.password)
          .then(res => {
            console.log("res", res);
          })
          .catch(err => {
            // console.error(err);
            // code: "auth/user-not-found" | code: "auth/wrong-password"
            alert("User not found or wrong password !")
          });
      },
      handleAuthStateChanged(store) {
        console.log("handleAuthStateChanged");
        firebaseAuth.onAuthStateChanged(user => {
          console.log("AuthStateChanged");
          if (user) {
            const userID = firebaseAuth.currentUser.uid;

            // user signed in, because user have value (not empty) so return true
            console.log("user signed in");
            // get user info from db
            // once: read data once, ie: want a snapshot of your data WITHOUT listening for changes
            firebaseDB.ref("users/" + userID).once("value", snapshot => {
              const userInfo = snapshot.val();

              console.log("snapshot", snapshot);
              console.log("userInfo", userInfo);
              store.commit("setUserInfo", {
                id: userID,
                ...userInfo
              });
              this.$router.push("/book");
            });
          } else {
            // user signed out
            console.log("user signed out");
            // clear user info, for next user sign in
            store.commit("clearUserInfo");
            // 跟 router.push 很像，唯一的不同就是，它不会向 history 添加新记录，而是跟它的方法名一样 —— 替换掉当前的 history 记录。
            this.$router.replace("/");
          }
        });
      },
      logoutUser() {
        console.log("logoutUser");
        firebaseAuth.signOut();
      },
      setBookmarks(store, bookmarks) {
        console.log("setBookmarks");
        store.state.bookmarks = bookmarks;
      },
      toogleIsSortDateAsc(store) {
        console.log("toogleIsSortDateAsc");
        store.commit("setIsSortDateAsc", !store.state.queryOption.isSortDateAsc);
      },
      toogleIsShowFavouriteOnly(store) {
        store.commit("setIsShowFavouriteOnly", !store.state.queryOption.isShowFavouriteOnly);
      },
      searchName(store, name) {
        store.commit("setQueryOptionName", name);
      },
      advancedSearch(store, advancedSearchData) {
        store.commit("setQueryOption", advancedSearchData);
      },
      clearQuery(store) {
        // TODO: tags later need modify
        store.commit("initQueryOption");
      }
    }

    // enable strict mode (adds overhead!)
    // for dev mode only
    //strict: process.env.DEV
  })

  return Store
}
