
import * as firebase from "firebase/app";

console.log("firebaseInit")

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAczEhhtzG5ka2Be4OhOMBYamGaVKngAz4",
  authDomain: "bookmarker-y3s2.firebaseapp.com",
  databaseURL: "https://bookmarker-y3s2.firebaseio.com",
  projectId: "bookmarker-y3s2",
  storageBucket: "bookmarker-y3s2.appspot.com",
  messagingSenderId: "712050825465",
  appId: "1:712050825465:web:8b5f6bca0539d25a262a28"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

// set up firestore
export default firebaseApp.firestore();
